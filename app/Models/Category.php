<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ["id"];
    protected $table = "categories";

    protected $fillable = [
        'code',
        'name',
        'description',
        'active'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    public function scopeId($query, $request)
    {
        if($request->has('id')){
            $query->where('id', $request->id);
        }
    }
}
