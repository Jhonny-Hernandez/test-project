<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ["id"];
    protected $table = "rates";

    protected $fillable = [
        'start_date',
        'end_date',
        'value',
        'product_id',
        'active'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime'
    ];

    public function product()
    {
        return $this->belongsTo("App\Models\Product", "product_id");
    }

    public function scopeProduct($query, $request)
    {
        if($request->has('product')){
            $query->where('product_id', $request->product);
        }
    }

    public function scopeId($query, $request)
    {
        if($request->has('id')){
            $query->where('id', $request->id);
        }
    }
}
