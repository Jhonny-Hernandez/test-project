<?php

namespace App\Http\Controllers;

use App\Core\Rates\Rate;
use App\Models\Product;
use Illuminate\Http\Request;

class RateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

           /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $core = new Rate(new Request(["product"=>$id]));
        $list= $core->show()->with(["product"])->get();
        $product = Product::findOrFail($id);
        return view('rates.list', compact('list', 'product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $product = Product::findOrFail($id);
        return view('rates.create',compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $request->merge(["product"=>$id]);
        $core = new Rate($request);
        $core->create();
        return redirect("/products/$id/rates")->with('success', 'Rate created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $rateId)
    {
        $core = new Rate(new Request(["product"=>$id, "id"=>$rateId]));
        $rate= $core->show()
            ->first()
            ->load(["product"]);        
        return view('rates.view', compact('rate','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $rateId)
    {
        $core = new Rate(new Request(["product"=>$id, "id"=>$rateId]));
        $rate= $core->show()
            ->first()
            ->load(["product"]);
        return view('rates.edit', compact('rate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $rateId)
    {
        $core = new Rate($request);
        $core->edit($rateId);
        return redirect("/products/$id/rates")->with('success', 'Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$rateId)
    {
        $core = new Rate();
        $core->delete($rateId);
        return redirect("/products/$id/rates")->with('success', 'Category deleted successfully');
    }
}
