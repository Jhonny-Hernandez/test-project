<?php

namespace App\Core\Rates;

use App\Models\Rate as RateModel;
use Illuminate\Http\Request;

class Rate
{
    // Model object
    private $object;
    // Object request
    private $request;
    // initial validators
    private $validations=[
        'start_date'=>'required|date',
        'end_date'=>'required|date',
        'value'=>'required|numeric',
        'product'=>'required|integer|exists:products,id'
    ];

    public function __construct($request=null) {
        $this->request = $request;        
    }

    // validate request object
    private function isValid()
    {
        $this->request->validate($this->validations);
    }

    // list objects
    public function show($id=null)
    {
        $list = RateModel::where('active', true)
            ->Product($this->request)
            ->Id($this->request);
        return $list;
    }    

    // create rate in DB
    public function create()
    {
        // Validate object
        $this->isValid();

        // Create object into DB
        $this->object = new RateModel();
        $this->object->start_date = $this->request->start_date;
        $this->object->end_date = $this->request->end_date;
        $this->object->value = $this->request->value;
        $this->object->product_id=$this->request->product;
        $this->object->save();
    }

    // edit rate in DB
    public function update($id)
    {
        // Validate object
        $this->isValid();

        // update object into DB
        $this->object = RateModel::findOrFail($id);
        $this->object->start_date = $this->request->start_date;
        $this->object->end_date = $this->request->end_date;
        $this->object->value = $this->request->value;
        $this->object->save();
    }

    // partial edition rate in DB
    public function edit($id)
    {
        // Validate object
        $this->validations =[
            'start_date'=>'sometimes|required|date',
            'end_date'=>'sometimes|required|date',
            'value'=>'sometimes|required|numeric',
            'active'=>'sometimes|required|boolean'
        ];
        $this->isValid();

        // update object into DB
        $this->object = RateModel::findOrFail($id);
        if($this->request->has('start_date'))
            $this->object->start_date = $this->request->start_date;
        if($this->request->has('end_date'))
            $this->object->end_date = $this->request->end_date;
        if($this->request->has('value'))
            $this->object->value = $this->request->value;
        if($this->request->has('active'))
            $this->object->active = $this->request->active;
        $this->object->save();
    }

    // delete rate in DB
    public function delete($id)
    {
        // Create object into DB
        $this->object = RateModel::findOrFail($id);
        $this->object->active = false;
        $this->object->save();
    }
}
