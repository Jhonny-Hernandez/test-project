<?php

namespace App\Core\Products;

use App\Models\Product as ProductModel;
use App\Core\Products\CategoryTrait;
use Illuminate\Http\Request;

class Product
{
    use CategoryTrait;
    // Model object
    private $object;
    // Object request
    private $request;
    // initial validators
    private $validators=[
        'code'=>'required|integer',
        'name'=> 'required|string|max:64',
        'description'=>'required|string|max:256',
        'categories'=>'required|array|min:1',
        'categories.*'=>'required|integer|exists:categories,id',
    ];

    public function __construct($request=null) {
        $this->request = $request;
    }

    // validate request object
    private function isValid()
    {
        $this->request->validate($this->validators);
    }

    // list objects
    public function show()
    {
        $list = ProductModel::where('active', true)
            ->Id($this->request);            
        return $list;
    }    

    // create product in DB
    public function create()
    {
        
        // Validate object
        $this->isValid();

        // Create object into DB
        $this->object = new ProductModel();
        $this->object->code = $this->request->code;
        $this->object->name = $this->request->name;
        $this->object->description = $this->request->description;
        $this->object->save();

        // Attach categories to a product
        $this->attachCategories($this->request->categories);
    }

    // edit product in DB
    public function update($id)
    {
        // Validate object
        $this->isValid();

        // update object into DB
        $this->object = ProductModel::findOrFail($id);
        $this->object->code = $this->request->code;
        $this->object->name = $this->request->name;
        $this->object->description = $this->request->description;
        $this->object->save();
        // Attach categories to a product
        $this->attachCategories($this->request->categories);
    }
    
    // partial edition product in DB
    public function edit($id)
    {
        // Validate object
        $this->validations =[
            'code'=>'sometimes|required|integer',
            'name'=> 'sometimes|required|string|max:64',
            'description'=>'sometimes|required|string|max:256',
            'active'=>'sometimes|required|boolean'
        ];
        $this->isValid();

        // update object into DB
        $this->object = ProductModel::findOrFail($id);
        if($this->request->has('code'))
            $this->object->code = $this->request->code;
        if($this->request->has('name'))
            $this->object->name = $this->request->name;
        if($this->request->has('description'))
            $this->object->description = $this->request->description;
        if($this->request->has('active'))
            $this->object->active = $this->request->active;
        $this->object->save();
        // Attach categories to a product
        $this->attachCategories($this->request->categories);
    }

    // delete product in DB
    public function delete($id)
    {
        // Create object into DB
        $this->object = ProductModel::findOrFail($id);
        $this->object->active = false;
        $this->object->save();
    }
    
}
