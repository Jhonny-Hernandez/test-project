<?php

namespace App\Core\Products;

trait CategoryTrait {

    // Attach categories to a product
    public function attachCategories(array $categories)
    {
        if ($this->request->has('categories'))
            $this->object->categories()->sync($categories);
    }

}