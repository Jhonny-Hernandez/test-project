<?php

namespace App\Core\Categories;

use App\Models\Category as CategoryModel;
use Illuminate\Http\Request;

class Category
{
    // Model object
    public $object;
    // Object request
    private $request; 
    // initial validators
    private $validations=[
        'code'=>'required|integer',
        'name'=> 'required|string|max:64',
        'description'=>'required|string|max:256'
    ];

    public function __construct($request=null) {
        $this->request = $request;
    }

    // validate request object
    private function isValid()
    {
        $this->request->validate($this->validations);
    }

    // list objects
    public function show()
    {
        $list = CategoryModel::where('active', true)->Id($this->request);
        return $list;
    }

    // create category in DB
    public function create()
    {
        // Validate object
        $this->isValid();

        // Create object into DB
        $this->object = new CategoryModel();
        $this->object->code = $this->request->code;
        $this->object->name = $this->request->name;
        $this->object->description = $this->request->description;
        $this->object->save();
    }

    // edit category in DB
    public function update($id)
    {
        // Validate object
        $this->isValid();

        // update object into DB
        $this->object = CategoryModel::findOrFail($id);
        $this->object->code = $this->request->code;
        $this->object->name = $this->request->name;
        $this->object->description = $this->request->description;
        $this->object->save();
    }

    // partial edition category in DB
    public function edit($id)
    {
        // Validate object
        $this->validations =[
            'code'=>'sometimes|required|integer',
            'name'=> 'sometimes|required|string|max:64',
            'description'=>'sometimes|required|string|max:256',
            'active'=>'sometimes|required|boolean'
        ];
        $this->isValid();

        // update object into DB
        $this->object = CategoryModel::findOrFail($id);
        if($this->request->has('code'))
            $this->object->code = $this->request->code;
        if($this->request->has('name'))
            $this->object->name = $this->request->name;
        if($this->request->has('description'))
            $this->object->description = $this->request->description;
        if($this->request->has('active'))
            $this->object->active = $this->request->active;
        $this->object->save();
    }

    // delete category in DB
    public function delete($id)
    {
        // Create object into DB
        $this->object = CategoryModel::findOrFail($id);
        $this->object->active = false;
        $this->object->save();
    }

}
