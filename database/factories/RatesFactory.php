<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Rate;
use Faker\Generator as Faker;

$factory->define(Rate::class, function (Faker $faker) {
    return [
        'start_date'=>$faker->dateTime($max = 'now', $timezone = null),
        'end_date'=>$faker->dateTime($max = 'now', $timezone = null),
        'value'=>$faker->randomNumber(5, true),
        'product_id'=>1
    ];
});
