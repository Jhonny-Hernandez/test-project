<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'code'=>$faker->randomNumber(5, true),
        'name'=>$faker->name,
        'description'=>$faker->realText(200,2)
    ];
});
