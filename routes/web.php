<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('products','ProductController');
Route::resource('categories','CategoryController');
Route::get('products/{id}/rates/', 'RateController@index')->name('rate.index');
Route::get('products/{id}/rates/create', 'RateController@create')->name('rate.create');
Route::post('products/{id}/rates', 'RateController@store')->name('rate.store');
Route::get('products/{id}/rates/{rateId}', 'RateController@show')->name('rate.show');
Route::get('products/{id}/rates/{rateId}/edit', 'RateController@edit')->name('rate.edit');
Route::patch('products/{id}/rates/{rateId}', 'RateController@update')->name('rate.update');
Route::delete('products/{id}/rates/{rateId}', 'RateController@destroy')->name('rate.destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
