## Deploy with Docker

### Run docker compose

1. Enter project: `cd test-project`
2. Start docker container: `docker-compose up -d`

### Configurate database 

1. Configurate `.env` variables:
    
    ```
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=prueba
    DB_USERNAME=admin
    DB_PASSWORD=admin
    ```

2. Create tables in database: php artisan migrate.
3. create data in database: php artisan db:seed. 

### Run local server

Execute: `php artisan serve`
