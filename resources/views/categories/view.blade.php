@extends('welcome')
@section('content')
    <div class="row">
        <div class="col-lg-1">
            <a class="btn btn-primary" href="{{ url('categories') }}"> Back</a>
        </div>
    </div>
    <table class="table table-bordered">
        <tr>
            <th>Code:</th>
            <td>{{ $category->code }}</td>
        </tr>
        <tr>
            <th>Name:</th>
            <td>{{ $category->name }}</td>
        </tr>
        <tr>
            <th>Address:</th>
            <td>{{ $category->description }}</td>
        </tr>
 
    </table>
@endsection