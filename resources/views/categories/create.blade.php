@extends('welcome')
@section('content')
    <div class="row">
        <div class="col-lg-11">
            <h2>Add New Categories</h2>
        </div>
        <div class="col-lg-1">
            <a class="btn btn-primary" href="{{ url('categories') }}"> Back</a>
        </div>
    </div>
 
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('categories.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="txtFirstName">Code:</label>
            <input type="text" class="form-control" id="txtFirstName" placeholder="Category code" name="code">
        </div>
        <div class="form-group">
            <label for="txtFirstName">Name:</label>
            <input type="text" class="form-control" id="txtFirstName" placeholder="Category Name" name="name">
        </div>
        <div class="form-group">
            <label for="txtDescription">Description:</label>
            <textarea class="form-control" id="txtDescription" name="description" rows="10" placeholder="Description"></textarea>
        </div>
        <button type="submit" class="btn btn-default">Save</button>
    </form>
@endsection