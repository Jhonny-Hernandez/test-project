@extends('welcome')
@section('content')

<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Categories</h6>

        <div class="col-lg-1">
            <a class="btn btn-success" href="{{ route('categories.create') }}">Add</a>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th>code</th>
                    <th>name</th>
                    <th>description</th>
                    <th>active</th>
                    <th width="280px">Action</th>
                </tr>
                @foreach ($list as $item)
                <tr>
                    <td>{{ $item->code }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->active }}</td>
                    <td>
                        <form action="{{ route('categories.destroy',$item->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('categories.show',$item->id) }}">Show</a>
                            <a class="btn btn-primary" href="{{ route('categories.edit',$item->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>

@endsection