@extends('welcome')
@section('content')
    <div class="row">
        <div class="col-lg-11">
            <h2>Update Category</h2>
        </div>
        <div class="col-lg-1">
            <a class="btn btn-primary" href="{{ url('products') }}"> Back</a>
        </div>
    </div>
 
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="post" action="{{ route('products.update',$product->id) }}" >
        @method('PATCH')
        @csrf

        <div class="form-group">
            <label for="txtFirstName">Code:</label>
            <input type="text" class="form-control" id="txtFirstName" placeholder="Product code" name="code" value="{{ $product->code }}">
        </div>
        <div class="form-group">
            <label for="txtFirstName">Name:</label>
            <input type="text" class="form-control" id="txtFirstName" placeholder="Product Name" name="name" value="{{ $product->name }}">
        </div>
        <div class="form-group">
            <label for="txtDescription">Description:</label>
            <textarea class="form-control" id="txtDescription" name="description" rows="10" placeholder="Description"> {{$product->description}} </textarea>
        </div>
        <div class="form-group">
            <label for="txtDescription">Categories:</label>
            <select class="form-control" id="txtCategories" name="categories[]" multiple>
            @foreach ($categories as $category)
            <option value="{{$category->id}}"@if(in_array($category->id, $product->categories->pluck('id')->toArray()))selected="selected"@endif> {{$category->name}} </option>
            @endforeach                
            </select>
        </div>
        <button type="submit" class="btn btn-default">Save</button>
    </form>
@endsection