@extends('welcome')
@section('content')
    <div class="row">
        <div class="col-lg-11">
                <h2> Producto {{$product->name}} </h2>
        </div>
        <div class="col-lg-1">
            <a class="btn btn-primary" href="{{ url('products') }}"> Back</a>
        </div>
    </div>
    <table class="table table-bordered">
        <tr>
            <th>Code:</th>
            <td>{{ $product->code }}</td>
        </tr>
        <tr>
            <th>Name:</th>
            <td>{{ $product->name }}</td>
        </tr>
        <tr>
            <th>Description:</th>
            <td>{{ $product->description }}</td>
        </tr>
 
    </table>

    <div class="row">
        <div class="col-lg-11">
                <h2> Categories </h2>
        </div>
    </div>

    @foreach ($product->categories as $category)
        <table class="table table-bordered">
            <tr>
                <th>Code:</th>
                <td>{{ $category->code }}</td>
            </tr>
            <tr>
                <th>Name:</th>
                <td>{{ $category->name }}</td>
            </tr>
            <tr>
                <th>Description:</th>
                <td>{{ $category->description }}</td>
            </tr>
        </table>
    @endforeach  
@endsection