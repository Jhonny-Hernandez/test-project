@extends('welcome')
@section('content')
    <div class="row">
        <div class="col-lg-11">
            <h2>Add New Rate to {{$product->name}} </h2>
        </div>
        <div class="col-lg-1">
            <a class="btn btn-primary" href="{{ route('rate.index', $product->id) }}"> Back</a>
        </div>
    </div>
 
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('rate.store', $product->id) }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="txtFirstName">Start date:</label>
            <input type="datetime-local" class="form-control" id="txtFirstName" placeholder="Start date" name="start_date">
        </div>
        <div class="form-group">
            <label for="txtFirstName">End date:</label>
            <input type="datetime-local" class="form-control" id="txtFirstName" placeholder="End date" name="end_date">
        </div>
        <div class="form-group">
            <label for="txtFirstName">Value:</label>
            <input type="number" class="form-control" id="txtFirstName" placeholder="value" name="value">
        </div>
        <button type="submit" class="btn btn-default">Save</button>
    </form>
@endsection