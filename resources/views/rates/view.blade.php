@extends('welcome')
@section('content')
    <div class="row">
        <div class="col-lg-11">
                <h2>Rates</h2>
        </div>
        <div class="col-lg-1">
            <a class="btn btn-primary" href="{{ route('rate.index', $id) }}"> Back</a>
        </div>
    </div>
    <table class="table table-bordered">
        <tr>
            <th>Start date:</th>
            <td>{{ $rate->start_date }}</td>
        </tr>
        <tr>
            <th>End date:</th>
            <td>{{ $rate->end_date }}</td>
        </tr>
        <tr>
            <th>Value:</th>
            <td>{{ $rate->value }}</td>
        </tr>
 
    </table>
@endsection