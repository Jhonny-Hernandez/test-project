@extends('welcome')
@section('content')


<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Product {{$product->name}} rates</h6>

        <div class="col-lg-1">
            <a class="btn btn-success" href="{{ route('rate.create', $product->id) }}">Add</a>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th>Start_date</th>
                    <th>End_date</th>
                    <th>Value</th>
                    <th>active</th>
                    <th>Product</th>
                    <th width="280px">Action</th>
                </tr>
                @foreach ($list as $item)
                <tr>
                    <td> {{ $item->start_date }}</td>
                    <td>{{ $item->end_date }}</td>
                    <td>{{ $item->value }}</td>
                    <td>{{ $item->active }}</td>
                    <td>{{ $item->product->name }}</td>
                    <td>
                        <form action="{{ route('rate.destroy',['id'=>$product->id,'rateId'=>$item->id]) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('rate.show',['id'=>$product->id,'rateId'=>$item->id]) }}">Show</a>
                            <a class="btn btn-primary" href="{{ route('rate.edit',['id'=>$product->id,'rateId'=>$item->id]) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection